const { statement, invoice, plays } = require("./statementFunction");

test("testing statement function", () => {
  const expectedResult = `Statement for BigCo
 Hamlet: $650.00 (55 seats)
 As You Like It: $580.00 (35 seats)
 Othello: $500.00 (40 seats)
Amount owed is $1,730.00
You earned 47 credits
`;

  const statementString = statement(invoice, plays);

  expect(statementString).toEqual(expectedResult);
});

const invalidPlays = {
  hamlet: { name: "Hamlet", type: "dramat" },
  "as-like": { name: "As You Like It", type: "comedy" },
  othello: { name: "Othello", type: "tragedy" },
};

test("testing the invalid plays throw an Error", () => {
  expect(() => statement(invoice, invalidPlays)).toThrow(
    "unknown type: dramat"
  );
});
