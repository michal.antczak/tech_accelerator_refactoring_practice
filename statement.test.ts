//On a class we need an instance of the class to test the different methods.

import { Statement } from "./statement";
import {invoice, plays} from "./dummy_data"
 
test("testing statement class", () => {
  const expectedResult = `Statement for BigCo
 Hamlet: $650.00 (55 seats)
 As You Like It: $580.00 (35 seats)
 Othello: $500.00 (40 seats)
Amount owed is $1,730.00
You earned 47 credits
`; 
  const statementInstance = new Statement(invoice, plays)
  expect(statementInstance.getStatement()).toEqual(expectedResult);
});

const invalidPlays = {
  hamlet: { name: "Hamlet", type: "drama" },
  "as-like": { name: "As You Like It", type: "comedy" },
  othello: { name: "Othello", type: "tragedy" },
};

test("testing the invalid plays throw an Error", () => {
    const statementInstance = new Statement(invoice, invalidPlays)
  expect(() => statementInstance.getStatement()).toThrow(
    "unknown type: drama"
  );
});
