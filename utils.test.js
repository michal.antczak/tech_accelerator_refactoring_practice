const { calculateAmount } = require("./utils");

describe("calculateAmount", () => {
  describe("When the play type is comedy", () => {
    it("should return 30000 + 300 * audience when audience is below 21 ", () => {
      const audience = 20;
      const expectedResult = 30000 + 300 * audience;

      expect(calculateAmount(audience, "comedy")).toEqual(expectedResult);
    });

    it("should return 30000 + 300 * audience plus 10000 and 500 for each person above 20", () => {
      const audience = 25;
      const expectedResult =
        30000 + 300 * audience + (10000 + 500 * (audience - 20));

      expect(calculateAmount(audience, "comedy")).toEqual(expectedResult);
    });
  });
});
