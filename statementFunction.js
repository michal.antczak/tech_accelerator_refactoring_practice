const { invoice, plays } = require("./dummy_data");
const { format, calculateAmount, calculateCredits } = require("./utils");

function statement(invoice, plays) {
  let totalAmount = 0;
  let volumenCredits = 0;
  let result = `Statement for ${invoice.customer}\n`;

  for (let perf of invoice.performances) {
    let play = plays[perf.playID];
    let thisAmount = calculateAmount(perf.audience, play.type);

    // add frequent renter points
    volumenCredits += calculateCredits(perf.audience, play.type);

    // print line for this order
    result += ` ${play.name}: ${format(thisAmount)} (${perf.audience} seats)\n`;
    totalAmount += thisAmount;
  }

  result += `Amount owed is ${format(totalAmount)}\n`;
  result += `You earned ${volumenCredits} credits\n`;

  return result;
}

module.exports = { statement, invoice, plays };
