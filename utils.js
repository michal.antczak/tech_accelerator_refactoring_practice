const globalFormat = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
  minimumFractionDigits: 2,
}).format;

function format(amount) {
  return globalFormat(amount / 100);
}

function calculateCredits(audience, type) {
  let volumenCredits = Math.max(audience - 30, 0);
  // add extra credit for every five comedy attendees
  if ("comedy" === type) volumenCredits += Math.floor(audience / 5);
  return volumenCredits;
}

function calculateAmount(audience, type) {
  switch (type) {
    case "tragedy":
      return 40000 + Math.max((audience - 30) * 1000, 0);
    case "comedy":
      const baseAmount = 30000 + 300 * audience;
      if (audience > 20) {
        return baseAmount + 10000 + 500 * (audience - 20);
      }
      return baseAmount;
    default:
      throw new Error(`unknown type: ${type}`);
  }
}

module.exports = { format, calculateAmount, calculateCredits };
