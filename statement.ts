import {invoice,plays } from "./dummy_data"

interface IInvoice {
    customer: string;
    performances: IPerformance[];
}

interface IPerformance {
    playID: string;
    audience: number;
}

interface IPlay {
    name: string;
    type: string;
}

interface IPlays {
    [key: string]: IPlay; 
}

interface IStatement { 
    getStatement(): string 
}

enum playTypes {
    Comedy = "comedy",
    Tragedy = "tragedy",
}

 export class Statement implements IStatement {
    private formatter;
    constructor(private invoice: IInvoice, private plays: IPlays){
        this.invoice = invoice;
        this.plays = plays;
        this.formatter = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
  minimumFractionDigits: 2,
}).format;
    }
    getStatement(){ 
      let totalAmount = 0;
      let volumenCredits = 0;
      let result = `Statement for ${this.invoice.customer}\n`;
     
  for (let perf of this.invoice.performances) {
    let play = this.plays[perf.playID];
    let thisAmount = this.calculateAmount(perf.audience, play.type);

    // add frequent renter points
    volumenCredits += this.calculateCredits(perf.audience, play.type);

    // print line for this order
    result += ` ${play.name}: ${this.format(thisAmount)} (${perf.audience} seats)\n`;
    totalAmount += thisAmount;
  }

  result += `Amount owed is ${this.format(totalAmount)}\n`;
  result += `You earned ${volumenCredits} credits\n`;

  return result;
    }

private format(amount: number) {
  return this.formatter(amount / 100);
}

private calculateCredits(audience: number, type: string) {
  let volumenCredits = Math.max(audience - 30, 0);
  // add extra credit for every five comedy attendees
  if (playTypes.Comedy === type) volumenCredits += Math.floor(audience / 5);
  return volumenCredits;
}

private calculateAmount(audience: number, type: string) {
  switch (type) {
    case playTypes.Tragedy:
      return 40000 + Math.max((audience - 30) * 1000, 0);
    case playTypes.Comedy:
      const baseAmount = 30000 + 300 * audience;
      if (audience > 20) {
        return baseAmount + 10000 + 500 * (audience - 20);
      }
      return baseAmount;
    default:
      throw new Error(`unknown type: ${type}`);
  }
 }
}
